﻿// The initialize function must be run each time a new page is loaded.
(function () {
    Office.initialize = function (reason) {
        // If you need to initialize something you can do so here.
    };
})();

var dialog;
function showUrlDialog(event) {
    Office.context.ui.displayDialogAsync(window.location.origin + "/EnterUrl.html", { height: 5, width: 10 },
        function(asyncResult) {
            dialog = asyncResult.value;
            dialog.addEventHandler(Office.EventType.DialogMessageReceived, processUrlMessage);
        }
    );
    event.completed();
}

function processUrlMessage(arg) {
    if (arg.message != "Error") {
        Office.context.document.settings.set('portal', arg.message);
        Office.context.document.settings.saveAsync();    
    }
    
    dialog.close();
}