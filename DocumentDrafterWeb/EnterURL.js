﻿(function () {
    Office.initialize = function (reason) {
        // If you need to initialize something you can do so here.
    };
})();

function save() {
    var val = document.getElementById('txtURL').value;
    if (isURL(val)) {
        Office.context.ui.messageParent(val);
    } else {
        Office.context.ui.messageParent("Error");
    }
}

function isURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(str);
}
